const express = require('express');
const path = require('path');
const multer = require('multer');
const nanoid = require('nanoid');

const Cocktail = require('../models/Cocktail');
const User = require('../models/User');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const getMeanRating = ratings => {
  const reducer = (acc, item) => acc + item.rating;
  const sum = ratings.reduce(reducer, 0);
  const average = sum / ratings.length;

  if (isNaN(average)) {
    return 0;
  }

  return average;
};

const upload = multer({storage});

const createRouter = () => {
  const router = express.Router();

  router.post('/', [auth, upload.single('image')], async (req, res) => {
    if (!req.body.title || !req.file || !req.body.recipe || !req.body.ingredients) {
      return res.send({message: 'Fill all required fields'});
    }

    const cocktailData = {
      userId: req.user._id,
      title: req.body.title,
      image: req.file.filename,
      recipe: req.body.recipe,
      ingredients: JSON.parse(req.body.ingredients)
    };

    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();

    return res.send({message: 'Cocktail is on consideration moderator', cocktail});
  });

  router.get('/', async (req, res) => {
    const token = req.get('Token');

    const user = await User.findOne({token});

    if (!user || user.role === 'user') {
      const cocktails = await Cocktail.find({published: true});

      return res.send({cocktails});
    }

    const cocktails = await Cocktail
      .find()
      .populate({path: 'userId', select: 'username'});

    return res.send({cocktails});
  });

  router.get('/:id', async (req, res) => {
    const id = req.params.id;

    const cocktail = await Cocktail.findOne({_id: id});
    const average = getMeanRating(cocktail.ratings);

    return res.send({cocktail, average});
  });

  router.post('/:id', async (req, res) => {
    const id = req.params.id;

    const cocktail = await Cocktail.findOne({_id: id});

    await Cocktail.findOneAndUpdate({_id: id}, {$push: {ratings: req.body.ratingData}});

    const average = getMeanRating(cocktail.ratings);

    return res.send({cocktail, average});
  });

  router.post('/publish/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;

    await Cocktail.findOneAndUpdate({_id: id}, {$set: {published: true}});

    const cocktail = await Cocktail.findOne({_id: id});
    const average = getMeanRating(cocktail.ratings);

    return res.send({cocktail, average});
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;

    const foundCocktail = await Cocktail.findOne({_id: id});

    if (!req.user._id === foundCocktail.userId) {
      return res.send({message: 'Not access!'})
    }

    await foundCocktail.remove();

    const cocktails = await Cocktail
      .find()
      .populate({path: 'userId', select: 'username'});

    return res.send({cocktails});
  });

  return router;
};

module.exports = createRouter;