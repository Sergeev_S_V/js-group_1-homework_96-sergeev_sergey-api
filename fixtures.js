const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [User1, Admin] = await User.create({
    username: 'user1',
    password: '123',
    role: 'user'
  }, {
    username: 'admin',
    password: '123',
    role: 'admin'
  });

  db.close();
});

